package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane borderPane = new BorderPane();
        Region region = new Region();
        region.setStyle("-fx-background-color: black");
        region.setPrefWidth(200);
        borderPane.setLeft(region);
        BorderPane.setAlignment(region, Pos.CENTER);
        Scene scene = new Scene(borderPane, 1280, 720, Color.WHITESMOKE);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Test");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
